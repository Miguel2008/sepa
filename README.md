# README #


validarSEPA_xml
Validar documento SEPA

## Validar documento SEPA ##

Consulte los archivos XML para los mensajes de inicio de pagos por domiciliación bancaria y transferencia de crédito de la Zona Única de Pagos en Euros (SEPA). Estos mensajes SEPA XML son un subconjunto del "esquema de mensajes de la industria financiera universal ISO20022".

Licencia: Licencia pública general reducida GNU v3.0

Las versiones del estándar seguidas son:

pain.001.002.03 (o pain.001.001.03) para transferencias de crédito y pain.008.002.02 (o pain.008.001.02) para domiciliaciones bancarias

puede validar su archivo xml aquí


### Documento_EPC132-08_SCT_C2B_2019_V1.0 ###

* [validar documento SEPA](https://documentosepa.eu/validar)




